package config

import (
	"flag"
	"fmt"
	"io/ioutil"

	"gitlab.com/djacobs24/comment-catcher/notify"
	"gitlab.com/djacobs24/comment-catcher/notify/email"
	"gitlab.com/djacobs24/comment-catcher/notify/stdout"
	"gopkg.in/yaml.v2"
)

type notifier string

const (
	stdoutNotifier notifier = "stdout"
	emailNotifier  notifier = "email"
)

const (
	defaultRepoPath   = "./..."
	defaultConfigPath = ".comment-catcher.yaml"
	defaultTimeout    = 60
	defaultNotifier   = stdoutNotifier
)

// Config holds all configurations needed to run the application.
type Config struct {
	TimeoutInSeconds int                 `yaml:"timeout_in_seconds"`
	HandleComments   map[string][]string `yaml:"comments"`
	Notify           NotifyConfig        `yaml:"notify"`
	Parse            ParseConfig         `yaml:"parse"`
}

// NotifyConfig holds the configurations specific to notifying the user.
type NotifyConfig struct {
	Notifier       notify.Notifier
	Type           notifier `yaml:"type"`
	DuplicateLines bool     `yaml:"duplicate_lines"`
}

// ParseConfig holds the configurations specific to parsing the comments.
type ParseConfig struct {
	Paths         []string
	ExcludePaths  []string `yaml:"excludes"`
	CaseSensitive bool     `yaml:"case_sensitive"`
}

// New constructs a new config.
func New() (Config, error) {
	cfg := Config{
		TimeoutInSeconds: defaultTimeout,
		Notify: NotifyConfig{
			Type: defaultNotifier,
		},
		Parse: ParseConfig{},
	}

	// Get the path of the config file.
	configPath := flag.String("config", defaultConfigPath, "path to the yaml configuration file")
	flag.Parse()

	// Read and parse the config file.
	yamlFile, err := ioutil.ReadFile(*configPath)
	if err != nil {
		return cfg, fmt.Errorf("error reading file from %s: %v", *configPath, err)
	}
	if err := yaml.Unmarshal(yamlFile, &cfg); err != nil {
		return cfg, fmt.Errorf("error unmarshaling the config: %v", err)
	}

	// Get the path of the repo to parse from the first argument.
	cfg.Parse.Paths = flag.Args()
	if len(cfg.Parse.Paths) == 0 {
		cfg.Parse.Paths = []string{defaultRepoPath}
	}

	switch cfg.Notify.Type {
	case emailNotifier:
		cfg.Notify.Notifier, err = email.New()
		if err != nil {
			return cfg, fmt.Errorf("error constructing email notifier: %v", err)
		}
	default:
		cfg.Notify.Notifier = stdout.New(cfg.Notify.DuplicateLines)
	}

	return cfg, nil
}
