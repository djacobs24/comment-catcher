# Comment Catcher
Comment Catcher is a simple tool to help developers remember to go back and handle those well-intentioned comments in their Go code.

## Configurations
Comment Catcher utilizes a YAML config file for its configurations. Check out the [example](./example/.comment-catcher.yaml) for all configuration options, then use the `--config` flag to point to your config file's location.

## Quick Start
`comment-catcher`: Same as `comment-catcher ./...`. Recursively parse the current directory, using the `.comment-catcher.yaml` configuration file located in the same directory.

`comment-catcher dir1/... dir2 dir3/file1.go`: Parse the specified directories and files. **NOTE** directories must include `/...` to be parsed recursively.

`comment-catcher --config "path/to/config.yaml" .`: Recursively parse the current directory, using `config.yaml` located at `path/to/config.yaml`.

## Features
- [x] Sensible defaults
- [x] Exclude files/folders
- [x] De-duplication option
- [x] Case sensitive option
- [x] Timeout
- [x] Recursive and non-recursive parsing options

## What's Coming?
- [ ] Allow flags instead of exclusively using a config
- [ ] Notify by email
