package main

import (
	"context"
	"errors"
	"log"
	"os"
	"time"

	"gitlab.com/djacobs24/comment-catcher/config"
	"gitlab.com/djacobs24/comment-catcher/notify"
	"gitlab.com/djacobs24/comment-catcher/parse"
)

func main() {
	// Get the configurations
	cfg, err := config.New()
	if err != nil {
		log.Fatalf("error getting config: %v", err)
	}

	// Create a context with a timeout
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(cfg.TimeoutInSeconds))
	defer cancel()

	// Parse the comments for each handle
	handleToMsgs := map[string][]notify.Message{}
	for handle, searchTerms := range cfg.HandleComments {
		msgs, err := parse.GetMessages(ctx, cfg.Parse.Paths, cfg.Parse.ExcludePaths, searchTerms, cfg.Parse.CaseSensitive)
		if err != nil {
			if errors.Is(err, parse.ErrDeadlineExceeded) {
				log.Fatalf("%d second timeout exceeded", cfg.TimeoutInSeconds)
			}
			log.Fatalf("error getting messages for '%s': %v", handle, err)
		}
		if len(msgs) > 0 {
			handleToMsgs[handle] = msgs
		}
	}

	// Notify the user
	numMsgs, err := cfg.Notify.Notifier.Send(ctx, handleToMsgs)
	if err != nil {
		log.Fatalf("error sending notification: %v", err)
	}

	// Exit with proper code
	exit(numMsgs)
}

func exit(numMsgs int) {
	if numMsgs > 0 {
		os.Exit(1)
	}

	os.Exit(0)
}
