package parse

import (
	"context"
	"errors"
	"fmt"
	"go/parser"
	"go/token"
	"io/fs"
	"io/ioutil"
	"path/filepath"
	"strings"

	"gitlab.com/djacobs24/comment-catcher/notify"
)

const (
	goSuffix        = ".go"
	recursiveSuffix = "/..."
)

// ErrDeadlineExceeded is the error used for timeouts.
var ErrDeadlineExceeded = errors.New("deadline exceeded")

// GetMessages retrieves all messages.
func GetMessages(ctx context.Context, paths, excludes, searchTerms []string, caseSensitive bool) ([]notify.Message, error) {
	// Placeholder for all messages found
	messages := []notify.Message{}

	// Loop over each search term.
	for _, term := range searchTerms {

		// Loop over each path
		for _, path := range paths {

			var msgs []notify.Message
			var err error
			if recurse(path) {
				msgs, err = parseDirRecursively(ctx, path, term, excludes, caseSensitive)
			} else {
				msgs, err = parseDir(ctx, path, term, excludes, caseSensitive)
			}
			if err != nil {
				return messages, fmt.Errorf("error parsing '%s': %w", path, err)
			}

			messages = append(messages, msgs...)
		}
	}

	return messages, nil
}

// parseDir parses a directory without recursing through subdirectories.
func parseDir(ctx context.Context, path, match string, excludes []string, caseSensitive bool) ([]notify.Message, error) {
	msgs := []notify.Message{}

	info, err := ioutil.ReadDir(path)
	if err != nil {
		return msgs, fmt.Errorf("error reading dir: %v", err)
	}

	for _, i := range info {
		if !i.IsDir() && isGoFile(i.Name()) {
			file := filepath.Join(path, i.Name())
			ms, err := parseFile(ctx, file, match, caseSensitive)
			if err != nil {
				return msgs, fmt.Errorf("error parsing file at '%s': %w", path, err)
			}
			msgs = append(msgs, ms...)
		}
	}

	return msgs, nil
}

// parseDirRecursively parses the directory including all subdirectories.
func parseDirRecursively(ctx context.Context, path, match string, excludes []string, caseSensitive bool) ([]notify.Message, error) {
	msgs := []notify.Message{}

	if path == "" {
		return msgs, nil
	}

	path = strings.TrimSuffix(path, recursiveSuffix)

	if err := filepath.WalkDir(path,
		func(path string, d fs.DirEntry, err error) error {
			if err != nil {
				return err
			}

			if d.IsDir() {
				return nil
			}

			if !isGoFile(path) {
				return nil
			}

			for _, exclude := range excludes {
				if strings.Contains(path, exclude) {
					return nil
				}
			}

			ms, err := parseFile(ctx, path, match, caseSensitive)
			if err != nil {
				return fmt.Errorf("error parsing file at '%s': %w", path, err)
			}
			msgs = append(msgs, ms...)

			return nil
		}); err != nil {
		return msgs, err
	}

	return msgs, nil
}

func parseFile(ctx context.Context, path, match string, caseSensitive bool) ([]notify.Message, error) {
	msgs := []notify.Message{}

	if ctx.Err() != nil {
		// Timeout
		return msgs, ErrDeadlineExceeded
	}

	// Parse the comment in the file
	fs := token.NewFileSet()
	file, err := parser.ParseFile(fs, path, nil, parser.ParseComments)
	if err != nil {
		return msgs, fmt.Errorf("error parsing file: %v", err)
	}

	// Loop over all the comments in the file
	for _, comment := range file.Comments {
		text := comment.Text()
		if !caseSensitive {
			text = strings.ToLower(text)
			match = strings.ToLower(match)
		}
		if strings.Contains(text, match) {
			absolutePosition := fs.PositionFor(comment.Pos(), false)
			msg := notify.Message{
				Path:     path,
				Text:     comment.Text(),
				Column:   int(absolutePosition.Column),
				Line:     int(absolutePosition.Line),
				Offset:   int(absolutePosition.Offset),
				Position: int(comment.Pos()),
			}
			msgs = append(msgs, msg)
		}
	}

	return msgs, nil
}

// isGoFile returns true if the provided path is a go file.
func isGoFile(path string) bool {
	return strings.HasSuffix(path, goSuffix)
}

// recurse returns true if the provided path ends in  '/...'.
func recurse(path string) bool {
	return strings.HasSuffix(path, recursiveSuffix)
}
