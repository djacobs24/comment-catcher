package email

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/djacobs24/comment-catcher/notify"
)

const (
	mime = "MIME-version: 1.0;\nContent-Type: text/plain; charset=\"UTF-8\";\n\n"
)

// New constructs a new SMTP.
// TODO(djacobs24): Figure out what this needs.
func New() (*SMTP, error) {
	return &SMTP{}, nil
}

// SMTP holds the dependencies needed to notify via SMTP.
type SMTP struct {
	host     string `yaml:"host"`
	port     string `yaml:"port"`
	from     string `yaml:"from"`
	password string `yaml:"password"`
}

// addr constructs the SMTP address.
func (s *SMTP) addr() string {
	return fmt.Sprintf("%s:%s", s.host, s.port)
}

// Send sends a notification via SMTP.
// TODO(djacobs24): Implement this.
func (s *SMTP) Send(ctx context.Context, recipientToMessages map[string][]notify.Message) (int, error) {
	// auth := smtp.PlainAuth("", e.from, e.password, e.host)
	// msg := []byte("Subject: " + subject + "\n" + mime + "\n" + body)
	// return smtp.SendMail(e.addr(), auth, e.from, []string{"djacobs24@outlook.com"}, msg)

	return 0, errors.New("unimplemented")
}
