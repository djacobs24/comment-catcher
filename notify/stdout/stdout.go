package stdout

import (
	"context"
	"fmt"

	"gitlab.com/djacobs24/comment-catcher/notify"
)

const (
	lineBreak = "--------------------------------------------------------------------------------"
)

// New constructs a new STDOUT.
func New(showDupeLines bool) *STDOUT {
	return &STDOUT{
		showDupeLines: showDupeLines,
	}
}

// STDOUT holds the dependencies needed to notify via standard output.
type STDOUT struct {
	showDupeLines bool
}

// Send sends a notification to standard out.
func (s *STDOUT) Send(ctx context.Context, recipientToMessages map[string][]notify.Message) (int, error) {
	if len(recipientToMessages) == 0 {
		// No messages to send, return.
		return 0, nil
	}

	// The full message to be returned.
	var msg string

	// The number of messages sent.
	var numMsgs int

	// Loop over each recipients messages.
	for recipient, messages := range recipientToMessages {
		if len(messages) == 0 {
			// The recipient has no messages, continue.
			continue
		}

		pathWithMessages := map[string][]notify.Message{}

		// Append each message to the path.
		for _, message := range messages {
			pathWithMessages[message.Path] = append(pathWithMessages[message.Path], message)
		}

		// Add the recipient with a line break to the final message.
		msg += fmt.Sprintf("%s\n%s\n", lineBreak, lineBreakWithRecipient(recipient))

		// Loop over each path.
		for path, messages := range pathWithMessages {
			var linesPrinted []int

			// Add the line to the final message.
			for _, message := range messages {
				if s.showDupeLines {
					// We're showing duplicates, add every line to the message.
					msg += fmt.Sprintf("%s:%d: %s", path, message.Line, message.Text)
					numMsgs++
					continue
				}

				if !containsInt(linesPrinted, message.Line) {
					// We're not showing duplicates, only add the line if it hasn't already been added.
					linesPrinted = append(linesPrinted, message.Line)
					msg += fmt.Sprintf("%s:%d: %s", path, message.Line, message.Text)
					numMsgs++
				}
			}
		}

		msg += "\n\n"
	}

	fmt.Println(msg)

	return numMsgs, nil
}

// lineBreakWithRecipient combines the line break with the recipient.
func lineBreakWithRecipient(recipient string) string {
	return fmt.Sprintf("%s %s", recipient, lineBreak[len(recipient)+1:])
}

// containsInt returns true if the slice of ints already contains the int.
func containsInt(haystack []int, needle int) bool {
	for _, h := range haystack {
		if h == needle {
			return true
		}
	}

	return false
}
