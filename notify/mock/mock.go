package mock

import (
	"context"

	"gitlab.com/djacobs24/comment-catcher/notify"
)

// Notifier mocks notify.Notifier.
type Notifier struct {
	NumMsgs int
	Error   error
}

// Send ...
func (n *Notifier) Send(ctx context.Context, recipientToMessages map[string][]notify.Message) (int, error) {
	return n.NumMsgs, n.Error
}
