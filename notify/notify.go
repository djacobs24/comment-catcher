package notify

import (
	"context"
)

type Notifier interface {
	Send(ctx context.Context, recipientToMessages map[string][]Message) (int, error)
}

type Message struct {
	Text     string
	Path     string
	Line     int
	Column   int
	Offset   int
	Position int
}

type ErrFoundComments struct {
	Msg string
}

func (e ErrFoundComments) Error() string {
	return e.Msg
}
