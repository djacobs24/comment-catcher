.PHONY: build
build:
	go build ./cmd/comment-catcher

.PHONY: example
example:
	go run ./ --config ./example/.comment-catcher.yaml

.PHONY: run
run:
	./comment-catcher
